﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Net;

namespace ScrapHtml.Core
{
    public class LireScanScrapper : AbstractScrapper
    {
        protected override string Domain
        {
            get
            {
                return @"http://m.lirescan.net{0}";
            }
        }

        protected override string URL
        {
            get
            {
                return @"http://m.lirescan.net/{0}-lecture-en-ligne/{2}/{1}";
            }
        }

        public override string URLList
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public LireScanScrapper(string name)
            :base(name)
        {
            
        }

        public override bool Scrap(HtmlDocument doc, ref byte[] data)
        {
            var chapterNode = doc.DocumentNode.SelectSingleNode("//select[@id=\"chapitres\"]");
            if (chapterNode.Elements("option").LongCount() < _chapter)
                return false;

            var node = doc.DocumentNode.SelectSingleNode("//img[@id=\"image_scan\"]");

            if (node == null)
                return false;

            string value = node.GetAttributeValue("src", "");

            if (String.IsNullOrWhiteSpace(value))
                return false;

            getImage(value, ref data);

            var pageNode = doc.DocumentNode.SelectSingleNode("//select[@id=\"pages\"]");
            if (pageNode.Elements("option").LongCount() <= _page)
                return false;

            return true;
        }

        public override List<KeyValuePair<string, string>> ScrapList(HtmlDocument doc)
        {
            throw new NotImplementedException();
        }
    }
}
