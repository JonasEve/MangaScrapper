﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ScrapHtml.Core
{
    public class HtmlLoader
    {
        private AbstractScrapper scrapper;
        private string path;

        public HtmlLoader(AbstractScrapper scrapper)
        {
            this.scrapper = scrapper;
        }

        public void ScrapContentToLocal(bool razChap, int offsetVolume = 1, int maxVolume = 0, int offsetChapter = 1)
        {
            if (razChap)
                while (ScrapContentToLocal(offsetChapter, 0, 0, "chap", offsetVolume++) > 0 && offsetVolume != (maxVolume + 1)) ;
            else
                while(offsetChapter - (offsetChapter = ScrapContentToLocal(offsetChapter, 0, 0, "chap", offsetVolume++)) > 0 && offsetVolume != (maxVolume + 1));

            if(offsetVolume <= maxVolume)
                Directory.Delete(path, true);
        }

        public int ScrapContentToLocal(int offsetChapter = 1, int maxChapter = 0, int? groupInterval = null, string scanType = "chap", int? volume = null)
        {
            path = @"C:\Users\User\Desktop\manga\" + scrapper.Name;
            Directory.CreateDirectory(path);
            if (volume.HasValue)
            {
                path += @"\" + scrapper.Name + "_volume" + volume;
                Directory.CreateDirectory(path);
            }

            scrapper.Path = path;

            Directory.CreateDirectory(path + @"\" + scrapper.Name + "_temp");

            HtmlDocument doc = new HtmlDocument();
            int chapter = offsetChapter;
            int page = 0;
            bool againChap = true;

            while (againChap)
            {
                do
                {
                    page++;

                    HttpClient webClient = new HttpClient();

                    try
                    {
                        byte[] data = webClient.GetByteArrayAsync(scrapper.GetUrl(page, chapter, volume ?? 0)).Result;
                        string download = Encoding.UTF8.GetString(data);
                        doc.LoadHtml(download);
                    }
                    catch (Exception e)
                    {
                        break;
                    }

                } while (scrapper.Scrap(doc));

                manageSaving(ref offsetChapter, maxChapter, groupInterval, scanType, chapter, page, ref againChap);

                page = 0;
                chapter++;
            }

            return chapter == maxChapter ? chapter : chapter - 1;
        }

        private void manageSaving(ref int offsetChapter, int maxChapter, int? groupInterval, string scanType, int chapter, int page, ref bool againChap)
        {
            if (page == 1 || chapter == maxChapter)
            {
                int final = chapter == maxChapter ? chapter : chapter - 1;

                if (groupInterval.HasValue)
                {
                    Directory.Move(path + @"\" + scrapper.Name + "_temp", path + @"\" + scrapper.Name + "_" + scanType + "_" + offsetChapter + (offsetChapter != final ? "_" + final : "" ));
                }
                else
                {
                    if (chapter == maxChapter)
                        Directory.Move(path + @"\" + scrapper.Name + "_temp", path + @"\" + scrapper.Name + "_" + scanType + "_" + chapter);
                    else
                        Directory.Delete(path + scrapper.Name + @"\" + scrapper.Name + "_temp");
                }
                againChap = false;
            }
            else
            {
                if (groupInterval.HasValue && groupInterval.Value != 0 &&chapter % groupInterval == 0)
                {
                    Directory.Move(path + @"\" + scrapper.Name + "_temp", path + @"\" + scrapper.Name + "_" + scanType + "_" + offsetChapter + (offsetChapter != chapter ? "_" + chapter : ""));
                    offsetChapter = chapter + 1;
                    Directory.CreateDirectory(path + @"\" + scrapper.Name + "_temp");
                }
                else if (!groupInterval.HasValue)
                {
                    Directory.Move(path + @"\" + scrapper.Name + "_temp", path + @"\" + scrapper.Name + "_" + scanType + "_" + chapter);
                }
            }
        }

        public List<KeyValuePair<string, string>> ScrapList()
        {
            HttpClient webClient = new HttpClient();
            HtmlDocument doc = new HtmlDocument();

            try
            {
                byte[] data = webClient.GetByteArrayAsync(scrapper.URLList).Result;
                string download = Encoding.UTF8.GetString(data);
                doc.LoadHtml(download);
                return scrapper.ScrapList(doc);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public byte[] ScrapContent(int chapter, int page)
        {
            HttpClient webClient = new HttpClient();
            HtmlDocument doc = new HtmlDocument();
            byte[] bytes = null;

            try
            {
                byte[] data = webClient.GetByteArrayAsync(scrapper.GetUrl(chapter, page)).Result;
                string download = Encoding.UTF8.GetString(data);
                doc.LoadHtml(download);
                scrapper.Scrap(doc, ref bytes);
            }
            catch (Exception e)
            {
                return bytes;
            }

            return bytes;
        }
    }
}
