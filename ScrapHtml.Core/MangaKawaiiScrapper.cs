﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace ScrapHtml.Core
{
    public class MangaKawaiiScrapper : AbstractScrapper
    {
        public MangaKawaiiScrapper(string name) : base(name)
        {
        }

        public override string URLList
        {
            get
            {
                return @"http://www.mangakawaii.com/liste-mangas";
            }
        }
     
        protected override string URL
        {
            get
            {
                return @"http://www.mangakawaii.com/uploads/manga/{0}/chapters/{2}/{1}.jpg";
            }
        }

        public override bool Scrap(HtmlDocument doc, ref byte[] data)
        {
            getImage(GetUrl(_chapter, _page), ref data);

            return true;
        }

        public override List<KeyValuePair<string, string>> ScrapList(HtmlDocument doc)
        {
            throw new NotImplementedException();
        }

        public override string GetUrl(params int[] param)
        {
            _page = param.Length > 0 ? param[0] : 0;
            _chapter = param.Length > 1 ? param[1] : 0;
            return string.Format(URL, Name, _page.ToString("00"), _chapter);
        }
    }
}
