﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ScrapHtml.Core
{
    public abstract class AbstractScrapper
    {
        protected int _chapter;
        protected int _page;

        public string Name { get; private set; }

        public abstract string URLList { get; }

        protected abstract string URL { get; }

        protected virtual string Domain { get { return "{0}"; } }  

        public string Path { get; set; }

        public AbstractScrapper(string name)
        {
            Name = name;
        }

        public bool Scrap(HtmlDocument doc)
        {
            byte[] bytes = null;
            return Scrap(doc, ref bytes);
        }

        public abstract bool Scrap(HtmlDocument doc, ref byte[] data);

        public abstract List<KeyValuePair<string, string>> ScrapList(HtmlDocument doc);

        public virtual string GetUrl(params int[] param)
        {
            _page = param.Length > 0 ? param[0] : 0;
            _chapter = param.Length > 1 ? param[1] : 0;
            return string.Format(URL, Name, _page, _chapter);
        }

        protected void getImage(string value, ref byte[] bytes)
        {
            if (bytes == null)
            {
                getImage(value);
                return;
            }

            string imageSrc = string.Format(Domain, value);

            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage response = client.GetAsync(imageSrc).Result)
                {
                    response.EnsureSuccessStatusCode();

                    string imageName = string.Format("{0}_{1}_{2}", Name, _chapter, _page);

                    bytes = response.Content.ReadAsByteArrayAsync().Result;
                }
            }
        }

        protected async void getImage(string value)
        {
            string imageSrc = string.Format(Domain, value);

            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage response = client.GetAsync(imageSrc).Result)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                        return;

                    string imageName = string.Format("{0}_{1}_{2}", Name, _chapter, _page);
                    using (Stream contentStream = await response.Content.ReadAsStreamAsync(), fileStream = new FileStream(Path + @"\" + Name + @"_temp\" + imageName + ".png", FileMode.Create, FileAccess.Write, FileShare.None, 8192, true))
                    {
                        var totalRead = 0L;
                        var totalReads = 0L;
                        var buffer = new byte[8192];
                        var isMoreToRead = true;

                        do
                        {
                            var read = await contentStream.ReadAsync(buffer, 0, buffer.Length);
                            if (read == 0)
                            {
                                isMoreToRead = false;
                            }
                            else
                            {
                                await fileStream.WriteAsync(buffer, 0, read);

                                totalRead += read;
                                totalReads += 1;

                                if (totalReads % 2000 == 0)
                                {
                                    Console.WriteLine(string.Format("total bytes downloaded so far: {0:n0}", totalRead));
                                }
                            }
                        }
                        while (isMoreToRead);
                    }
                }
            }
        }
    }
}
