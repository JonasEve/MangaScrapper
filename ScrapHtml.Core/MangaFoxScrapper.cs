﻿using System;
using System.Collections.Generic;
using System.Text;
using HtmlAgilityPack;

namespace ScrapHtml.Core
{
    //TODO : problem
    public class MangaFoxScrapper : AbstractScrapper
    {
        public MangaFoxScrapper(string name) : base(name)
        {
        }

        public override string URLList => throw new NotImplementedException();

        protected override string URL => "http://mangafox.me/manga/{0}/{3}/c{2}/{1}.html";

        public override bool Scrap(HtmlDocument doc, ref byte[] data)
        {
            var node = doc.DocumentNode.SelectSingleNode("//img[@id=\"image\"]");

            if (node == null)
                return false;

            string value = node.GetAttributeValue("src", "");

            if (String.IsNullOrWhiteSpace(value))
                return false;

            getImage(value, ref data);

            return true;
        }

        public override List<KeyValuePair<string, string>> ScrapList(HtmlDocument doc)
        {
            throw new NotImplementedException();
        }

        public override string GetUrl(params int[] param)
        {
            _page = param.Length > 0 ? param[0] : 0;
            _chapter = param.Length > 1 ? param[1] : 0;
            return string.Format(URL, Name, _page, _chapter.ToString("000"), param[2] != 0 ? "v" + param[2].ToString("00") : "");
        }
    }
}
