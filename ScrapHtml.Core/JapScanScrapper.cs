﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace ScrapHtml.Core
{
    public class JapScanScrapper : AbstractScrapper
    {
        private bool isTome;

        public JapScanScrapper(string name, bool isTome = false) : base(name)
        {
            this.isTome = isTome;
        }

        protected override string URL
        {
            get
            {
                return @"http://www.japscan.com/lecture-en-ligne/{0}/"+ (isTome ? "volume-" : "") + "{2}/{1}.html";
            }
        }

        public override string URLList
        {
            get
            {
                return @"http://www.japscan.com/mangas/";
            }
        }

        public override bool Scrap(HtmlDocument doc, ref byte[] data)
        {
            if (doc.DocumentNode.SelectSingleNode("//a[@id=\"active\"]") == null || 
                !doc.DocumentNode.SelectSingleNode("//a[@id=\"active\"]").InnerText.Contains(_page.ToString()))
                return false;

            var node = doc.DocumentNode.SelectSingleNode("//img[@id=\"image\"]");

            if (node == null)
                return false;

            string value = node.GetAttributeValue("src", "");

            if (String.IsNullOrWhiteSpace(value))
                return false;

            getImage(value, ref data);

            return true;
        }

        public override List<KeyValuePair<string, string>> ScrapList(HtmlDocument doc)
        {
            throw new NotImplementedException();
        }
    }
}
