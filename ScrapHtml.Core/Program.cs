﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScrapHtml.Core
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //string name = "platinum-end";
            //AbstractScrapper scrapper = new LireScanScrapper(name);

            //string name = "all-you-need-is-kill";
            //AbstractScrapper scrapper = new NihonScanScrapper(name);

            string name = "alice-on-border-road";
            AbstractScrapper scrapper = new JapScanScrapper(name, true);

            //string name = "darwins-game";
            //AbstractScrapper scrapper = new MangaFoxScrapper(name);

            //string name = "kingdom-heart-ii";
            //AbstractScrapper scrapper = new MangaKawaiiScrapper(name);

            //string name = "darwin_s_game";
            //AbstractScrapper scrapper = new MangaHereScrapper(name);

            new HtmlLoader(scrapper).ScrapContentToLocal(1, 0, 1, "tome");
        }
    }
}
