﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrapHtml.Core
{
    public class NihonScanScrapper : AbstractScrapper
    {
        protected override string URL
        {
            get
            {
                return @"http://nihonscan.com/{0}-scan-{1}/{2}";
            }
        }

        public override string URLList
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public NihonScanScrapper(string name)
            :base(name)
        {

        }

        public override bool Scrap(HtmlDocument doc, ref byte[] bytes)
        {
            var node = doc.DocumentNode.SelectSingleNode("//img[@class=\"img-responsive page-full-image\"]");

            if (node == null)
                return false;

            string value = node.GetAttributeValue("src", "");

            if (String.IsNullOrWhiteSpace(value))
                return false;

            getImage(value, ref bytes);

            return true;
        }

        public override List<KeyValuePair<string, string>> ScrapList(HtmlDocument doc)
        {
            throw new NotImplementedException();
        }
    }
}
