﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileToPng
{
    class DirectoryExplorer
    {
        private string path;
        public DirectoryExplorer(string path)
        {
            this.path = path;
        }

        public void StartExploration()
        {
            foreach(string directory in Directory.GetDirectories(path))
            {
                exploreDirectory(directory);
            }
        }

        private void exploreDirectory(string directory)
        {
            foreach (string file in Directory.GetFiles(directory))
            {
                new Converter("png").Convert(file);
            }

            foreach (string subDirectory in Directory.GetDirectories(directory))
            {
                exploreDirectory(subDirectory);
            }
        }
    }
}
