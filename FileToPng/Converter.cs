﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileToPng
{
    public class Converter
    {
        private string extensionName;

        public Converter(string extensionName)
        {
            this.extensionName = extensionName;
        }

        public void Convert(string file)
        {
            string filename = Path.ChangeExtension(file, extensionName);
            File.Move(file, filename);
        }
    }
}
